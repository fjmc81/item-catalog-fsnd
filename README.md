# fullstack-nanodegree-vm

Common code for the Relational Databases and Full Stack Fundamentals courses.

## Getting Started

This project is forked from Udacity (github) within the nanodegree program.

### Basic Info required for reviewer

* Site URL `http://54.93.104.229.xip.io`
* IP_address `54.93.104.229`
* SSH port `2200`

##  Built With

* Server build with `ubuntu-server 16.04`
* `apache2 web server`
* `python 2.7`
* `flask`
* `postgresql`

## Summary of configurations made

* Set up default user `grader`, giving him all privileges required for this project.
* Disable remote access for `root` and `ubuntu` linux system users.
* Enable and config ubuntu firewall (utf), disabling default port SSH 22 for 2200
* Set up `private` and `public` key making possible a secure connection between local machine and the server
* Install `aptitude` in ubuntu-server to update successfully the server.
* Install all software required to run the app (apache2 web server, postgresql, python, flask, etc)
* Upload the files needed by the app.
* Migrate database schema from SQLite to Postgresql
* Set up Google Api for Secure Authorization

## Contributing

This project has been made from this [repo](https://github.com/frank-carracedo/fullstack-nanodegree-vm)

## Versioning

v : 1.0.1

## Author

* **Francisco Carracedo** - *Owner* - [frank-carracedo](https://github.com/frank-carracedo)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

This project is using the following Third Party resources from:
* Google API --Authentication
* Amazon LightTail --Hosting
