from functools import wraps
from flask import Flask, render_template, request, redirect, jsonify, \
                    url_for, flash, session, g
from flask_login import current_user
from sqlalchemy import create_engine, asc
from sqlalchemy.orm import sessionmaker
from database_setup import Base, Catalog, CatItem, User
from flask import session as login_session
import random
import string
from oauth2client.client import flow_from_clientsecrets
from oauth2client.client import FlowExchangeError
import httplib2
import json
from flask import make_response
import requests

app = Flask(__name__)
app.secret_key = 'super_secret_key'

CLIENT_ID = json.loads(
    open('/var/www/catalog/catalog/client_secret.json', 'r').read())['web']['client_id']
APPLICATION_NAME = "Catalog App"

# Connect to Database and create database session
### engine = create_engine('sqlite:///catalogitems.db?check_same_thread=False')
engine = create_engine('postgresql://catalog:catalog@localhost/catalog')
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()


@app.route('/login')
def showLogin():
    """
    showLogin: Create anti-forgery state token
    Returns:
        return a login page in safer way
    """
    state = ''.join(random.choice(string.ascii_uppercase + string.digits)
                    for x in xrange(32))
    login_session['state'] = state
    return render_template('login.html', STATE=state)


def login_required(f):
    """
    login_required : Login Required Decorator as it is described in
        'http://flask.pocoo.org/docs/1.0/patterns/viewdecorators/'
    Args:
        arg1 f
    Returns: decorated_function
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'username' in login_session:
            return f(*args, **kwargs)
        else:
            flash("You are not allowed to access there")
            return redirect('/login')
    return decorated_function


@app.route('/catalog/<string:catalog_url>/JSON')
@login_required
def catalogJSON(catalog_url):
    """
    catalogJSON: It catches from database an element matched with arguments
                    passed through the function
    Args:
        arg1 (string): catalog_url
    Returns:
        return a json format output of catalog tree database
    """
    items = session.query(CatItem).filter_by(
        cat_url=catalog_url).all()
    return jsonify(CatItem=[item.serialize for item in items])


# ADD JSON ENDPOINT HERE
@app.route('/catalog/<string:catalog_url>/<string:catitem_url>/JSON')
@login_required
def catalogItemJSON(catalog_url, catitem_url):
    """
    catalogItemJSON: It catches from database an element matched with
                        arguments passed through the function
    Args:
        arg1 (string): catalog_url
        arg2 (string): catitem_url
    Returns:
        return a json format output of item catalog tree database
    """
    catItem = session.query(CatItem).filter_by(url=catitem_url).one()
    return jsonify(CatItem=catItem.serialize)


@app.route('/gconnect', methods=['POST'])
def gconnect():
    """
    gconnect: Validate the user credentials through Google ID API

    Returns:
        return an output depending if the process was sucess or not
    """
    # Validate state token
    if request.args.get('state') != login_session['state']:
        response = make_response(json.dumps('Invalid state parameter.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    # Obtain authorization code
    code = request.data

    try:
        # Upgrade the authorization code into a credentials object
        oauth_flow = flow_from_clientsecrets('/var/www/catalog/catalog/client_secret.json', scope='')
        oauth_flow.redirect_uri = 'postmessage'
        credentials = oauth_flow.step2_exchange(code)
    except FlowExchangeError:
        response = make_response(
            json.dumps('Failed to upgrade the authorization code.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Check that the access token is valid.
    access_token = credentials.access_token
    url = ('https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=%s'
           % access_token)
    h = httplib2.Http()
    result = json.loads(h.request(url, 'GET')[1])
    # If there was an error in the access token info, abort.
    if result.get('error') is not None:
        response = make_response(json.dumps(result.get('error')), 500)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Verify that the access token is used for the intended user.
    gplus_id = credentials.id_token['sub']
    if result['user_id'] != gplus_id:
        response = make_response(
            json.dumps("Token's user ID doesn't match given user ID."), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Verify that the access token is valid for this app.
    if result['issued_to'] != CLIENT_ID:
        response = make_response(
            json.dumps("Token's client ID does not match app's."), 401)
        print "Token's client ID does not match app's."
        response.headers['Content-Type'] = 'application/json'
        return response

    stored_access_token = login_session.get('access_token')
    stored_gplus_id = login_session.get('gplus_id')
    if stored_access_token is not None and gplus_id == stored_gplus_id:
        response = make_response(json.dumps('Current user is already \
                    connected.'), 200)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Store the access token in the session for later use.
    login_session['access_token'] = credentials.access_token
    login_session['gplus_id'] = gplus_id

    # Get user info
    userinfo_url = "https://www.googleapis.com/oauth2/v1/userinfo"
    params = {'access_token': credentials.access_token, 'alt': 'json'}
    answer = requests.get(userinfo_url, params=params)

    data = answer.json()

    login_session['username'] = data['name']
    login_session['picture'] = data['picture']
    login_session['email'] = data['email']

    if data['id'] == True:
        login_session['user_id'] = data['id']
        user_id = login_session['user_id']
    else:
        user_id = createUser(login_session)

    login_session['user_id'] = user_id

    output = ''
    output += '<h1>Welcome, '
    output += login_session['username']
    output += '!</h1>'
    output += '<img src="'
    output += login_session['picture']
    output += ' " style = "width: 300px; height: 300px;border-radius: 150px;\
                    -webkit-border-radius: 150px;-moz-border-radius: 150px;"> '
    flash("you are now logged in as %s" % login_session['username'])
    print "done!"
    return output


# DISCONNECT - Revoke a current user's token and reset their login_session
@app.route('/gdisconnect')
def gdisconnect():
    """
    gdisconnect: Destroy the user's google token and its session

    Returns:
        user (anonymous now) is bringing to index
    """
    access_token = login_session.get('access_token')
    if access_token is None:
        print 'Access Token is None'
        response = make_response(json.dumps('Current user not connected'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    print 'In gdisconnect access token is %s', access_token
    print 'User name is: '
    print login_session['username']
    url = 'https://accounts.google.com/o/oauth2/revoke?token=%s' % \
        login_session['access_token']
    h = httplib2.Http()
    result = h.request(url, 'GET')[0]
    print 'result is '
    print result
    if result['status'] == '200':
        del login_session['access_token']
        del login_session['gplus_id']
        del login_session['username']
        del login_session['email']
        del login_session['picture']
        response = make_response(json.dumps('Successfully disconnected.'), 200)
        response.headers['Content-Type'] = 'application/json'
        response = make_response(redirect(url_for('showCatalogs')))
        return response
    else:
        response = make_response(
            json.dumps('Failed to revoke token for given user.', 400)
            )
        response.headers['Content-Type'] = 'application/json'
        response = make_response(redirect(url_for('showCatalogs')))
        return response


# User Helper Functions
def createUser(login_session):
    """
    createUser: to keep some user credentials info stored in database.
    Args:
        login_session (object): user info manage by session
    Returns:
        an user id within our database after successful login process
    """
    newUser = User(name=login_session['username'], email=login_session[
                   'email'], picture=login_session['picture'])
    session.add(newUser)
    session.commit()
    user = session.query(User).filter_by(
                    email=login_session['email']
                    ).one_or_none()
    return user.id


def getUserInfo(user_id):
    """
    getUserInfo: to retrieve the user data that has been stored in database.
    Args:
        user_id (object): to identify the user that it has being searched
    Returns:
        the variable with the user info saved in the database.
    """
    user = session.query(User).filter_by(id=user_id).one()
    return user


def getUserID(email):
    """
    getUserID: to retrieve the user data that has been stored in database.
    Args:
        email (string): to identify the user that it has being searched
    Returns:
        the variable with the user info saved in the database.
    """
    user = session.query(User).filter_by(email=email).one()
    return user.id


# Show all catalogs
@app.route('/')
@app.route('/catalog/')
def showCatalogs():
    """
    showCatalogs: retrieve from database the list of all catalogs

    Returns:
        an ouput with the list of catalogs
    """
    catalogs = session.query(Catalog).order_by(asc(Catalog.id))
    lastitems = session.query(CatItem).order_by(asc(CatItem.id))
    return render_template(
        'catalogs.html',
        catalogs=catalogs,
        lastitems=lastitems
        )


# Create a new category
@app.route('/catalog/new/', methods=['GET', 'POST'])
@login_required
def newCatalog():
    """
    newCatalog: add new element in database populated by user

    Returns:
        return the user to the main view if everything was ok
    """
    if request.method == 'POST':
        newCatalog = Catalog(
            name=request.form['name'],
            url=request.form['url'],
            )
        session.add(newCatalog)
        flash('New Catalog %s Successfully Created' % newCatalog.name)
        session.commit()
        return redirect(url_for('showCatalogs'))
    else:
        return render_template('newCatalog.html')


# Edit a catalogs
@app.route('/catalog/<string:catalog_url>/edit/', methods=['GET', 'POST'])
@login_required
def editCatalog(catalog_url):
    """
    editCatalog: show an existing element being the user able to modify
                    the existing data stored by new one
    Args:
        catalog_url (string)
    Returns:
        bring the user to main view if everything was ok
    """
    if 'username' in login_session:
        if request.form['name']:
            editedCatalog.name = request.form['name']
            flash('Catalog Successfully Edited %s' % editedCatalog.name)
            return redirect(url_for('showCatalogs'))
    else:
        return render_template('editcatalog.html', catalog=editedCatalog)


# Delete a catalogs
@app.route('/catalog/<string:catalog_url>/delete/', methods=['GET', 'POST'])
@login_required
def deleteCatalog(catalog_url):
    """
    deleteCatalog: remove from the database the selected element by user
    Args:
        catalog_url (string)
    Returns:
        bring the user to main view if everything was ok
    """
    catalogToDelete = session.query(Catalog).filter_by(url=catalog_url).one()
    # if request.method == 'POST':
    if 'username' in login_session:
        session.delete(catalogToDelete)
        flash('%s Successfully Deleted' % catalogToDelete.name)
        session.commit()
        return redirect(url_for('showCatalogs', catalog_url=catalog_url))
    else:
        return render_template(
            'deletecatalogconfirmation.html',
            catalogs=catalogToDelete
            )


# Show a items
@app.route('/catalog/<string:catalog_url>/')
def showCatItemListed(catalog_url):
    """
    showCatItemListed: retrieve from database the list of all items from
                            specific catalog
    Args:
        catalog_url (string)
        catitem_url (string)
    Returns:
        an ouput with the list of items from specific catalog
    """
    catitems = session.query(CatItem).all()
    catitem_url = catalog_url
    return render_template(
        'catitemlisted.html',
        catalog_url=catitem_url,
        catitems=catitems
        )


@app.route('/catalog/<string:catalog_url>/<string:catitem_url>/')
def showCatItemDetailed(catalog_url, catitem_url):
    """
    showCatItemDetailed: retrieve from database the specific item
    Args:
        catalog_url (string)
        catitem_url (string)
    Returns:
        an ouput with all atributes contained in the item selected
    """
    catitems = session.query(CatItem).all()
    catitem_url = catitem_url
    catalog_url = catalog_url
    return render_template(
        'itemslisted.html',
        catitems=catitems,
        catitem_url=catitem_url,
        catalog_url=catalog_url
        )


# Create a new catalog item
@app.route('/catalog/<string:catalog_url>/new/', methods=['GET', 'POST'])
@login_required
def newItemCatalog(catalog_url):
    """
    newItemCatalog: add new element in database populated by user

    Returns:
        return the user to the main view if everything was ok
    """
    if request.method == 'POST':
        newItemCatalog = CatItem(
            name=request.form['name'], url=request.form['url'],
            description=request.form['description'],
            user_id=login_session['gplus_id'],
            cat_url=catalog_url
            )
        session.add(newItemCatalog)
        flash('New Item Catalog %s Successfully Created' % newItemCatalog.name)
        session.commit()
        return redirect(url_for(
            'showCatItemListed',
            catalog_url=catalog_url+newItemCatalog.url
            ))
    else:
        return render_template('newcatitemlisted.html')


# Edit a menu item
@app.route('/catalog/<string:catalog_url>/<string:catitem_url>/edit',
           methods=['GET', 'POST'])
@login_required
def editItemCatalog(catalog_url, catitem_url):
    """
    editCatalog: show an existing element being the user able to modify
                    the existing data stored by new one
    Args:
        catalog_url (string)
        catitem_url (string)
    Returns:
        bring the user to main view if everything was ok
    """
    editedItem = session.query(CatItem).filter_by(url=catitem_url).one()
    if request.method == 'POST':
        if request.form['name']:
            editedItem.name = request.form['name']
        if request.form['url']:
            editedItem.description = request.form['url']
        if request.form['description']:
            editedItem.price = request.form['description']
        session.add(editedItem)
        session.commit()
        return redirect(url_for('showCatItemListed', catalog_url=catalog_url))
    else:
        return render_template(
            'editcatitem.html',
            catalog_url=catalog_url,
            catitem_url=catitem_url,
            item=editedItem
            )


# Delete a catalog item
@app.route('/catalog/<string:catalog_url>/<string:catitem_url>/delete',
           methods=['GET', 'POST'])
@login_required
def deleteItemCatalog(catalog_url, catitem_url):
    """
    deleteCatalog: remove from the database the selected element by user
    Args:
        catalog_url (string)
        catitem_url (string)
    Returns:
        bring the user to main view if everything was ok
    """
    itemToDelete = session.query(CatItem).filter_by(url=catitem_url).one()
    if request.method == 'POST':
        session.delete(itemToDelete)
        session.commit()
        return redirect(url_for('showCatItemListed', catalog_url=catalog_url))
    else:
        return render_template(
            'deleteconfirmation.html',
            catalog_url=catalog_url,
            catitem_url=catitem_url
            )


# Disconnect based on provider
@app.route('/disconnect')
@login_required
def disconnect():
    """
    disconnect: make the logout of the user

    Returns:
        return token identifying the user is destroy.
    """
    if 'provider' in login_session:
        if login_session['provider'] == 'google':
            gdisconnect()
            flash("You have successfully been logged out.")
            return redirect(url_for('showCatalogs'))
    else:
        flash("You were not logged in")
        return redirect(url_for('showCatalogs'))

if __name__ == '__main__':
    app.run(host='0.0.0.0')
