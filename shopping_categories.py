from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from database_setup import Catalog, Base, CatItem

engine = create_engine('sqlite:///catalogitems.db')
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()

vegetables = Catalog(name='Vegetables', url='vegetables')
session.add(vegetables)
fruits = Catalog(name='Fruits', url='fruits')
session.add(fruits)
dairyproducts = Catalog(name='Dairy Products', url='dairy-products')
session.add(dairyproducts)
meat = Catalog(name='Meat', url='meat')
session.add(meat)
session.commit()


def get_cat(catalog_url):
    """Get catalog id for a given catalog url"""
    catalog = session.query(Catalog).filter_by(url=catalog_url).one()
    return catalog

DBSession = sessionmaker(bind=engine)
session = DBSession()
tomatoes = CatItem(
    name="tomatoes",
    description="Aliquam interdum nisi augue, ut malesuada nisi egestas non." +
                "Integer tincidunt lorem metus, ut congue ipsum congue at. " +
                "Nulla varius nec quam sit amet convallis. Nulla a posuere " +
                "ligula. Aenean sem odio, consectetur in molestie nec, " +
                "sodales vitae massa. Etiam in mollis tortor, gravida " +
                "aliquet nunc. Integer eu neque vitae diam laoreet commodo " +
                "semper sed felis.",
    price="1.50",
    sku="001",
    url="tomatoes",
    cat_url="vegetables"
    )

session.add(tomatoes)
lettuce = CatItem(
    name="lettuce",
    description="Aliquam interdum nisi augue, ut malesuada nisi egestas non." +
                "Integer tincidunt lorem metus, ut congue ipsum congue at. " +
                "Nulla varius nec quam sit amet convallis. Nulla a posuere " +
                "ligula. Aenean sem odio, consectetur in molestie nec, " +
                "sodales vitae massa. Etiam in mollis tortor, gravida " +
                "aliquet nunc. Integer eu neque vitae diam laoreet commodo " +
                "semper sed felis.",
    price="0.95",
    sku="002",
    url="lettuce",
    cat_url="vegetables"
    )

session.add(lettuce)
orange = CatItem(
    name="orange",
    description="Aliquam interdum nisi augue, ut malesuada nisi egestas non." +
                "Integer tincidunt lorem metus, ut congue ipsum congue at. " +
                "Nulla varius nec quam sit amet convallis. Nulla a posuere " +
                "ligula. Aenean sem odio, consectetur in molestie nec, " +
                "sodales vitae massa. Etiam in mollis tortor, gravida " +
                "aliquet nunc. Integer eu neque vitae diam laoreet commodo " +
                "semper sed felis.",
    price="3.50",
    sku="003",
    url="orange",
    cat_url="fruits"
    )

session.add(orange)
banana = CatItem(
    name="banana",
    description="Aliquam interdum nisi augue, ut malesuada nisi egestas non." +
                "Integer tincidunt lorem metus, ut congue ipsum congue at. " +
                "Nulla varius nec quam sit amet convallis. Nulla a posuere " +
                "ligula. Aenean sem odio, consectetur in molestie nec, " +
                "sodales vitae massa. Etiam in mollis tortor, gravida " +
                "aliquet nunc. Integer eu neque vitae diam laoreet commodo " +
                "semper sed felis.",
    price="1.50",
    sku="004",
    url="banana",
    cat_url="fruits"
    )

session.add(banana)
cherry = CatItem(
    name="cherry",
    description="Aliquam interdum nisi augue, ut malesuada nisi egestas non." +
                "Integer tincidunt lorem metus, ut congue ipsum congue at. " +
                "Nulla varius nec quam sit amet convallis. Nulla a posuere " +
                "ligula. Aenean sem odio, consectetur in molestie nec, " +
                "sodales vitae massa. Etiam in mollis tortor, gravida " +
                "aliquet nunc. Integer eu neque vitae diam laoreet commodo " +
                "semper sed felis.",
    price="5.50",
    sku="005",
    url="cherry",
    cat_url="fruits"
    )

session.add(cherry)
apple = CatItem(
    name="apple",
    description="Aliquam interdum nisi augue, ut malesuada nisi egestas non." +
                "Integer tincidunt lorem metus, ut congue ipsum congue at. " +
                "Nulla varius nec quam sit amet convallis. Nulla a posuere " +
                "ligula. Aenean sem odio, consectetur in molestie nec, " +
                "sodales vitae massa. Etiam in mollis tortor, gravida " +
                "aliquet nunc. Integer eu neque vitae diam laoreet commodo " +
                "semper sed felis.",
    price="2.50",
    sku="006",
    url="apple",
    cat_url="fruits"
    )

session.add(apple)
milk = CatItem(
    name="milk",
    description="Aliquam interdum nisi augue, ut malesuada nisi egestas non." +
                "Integer tincidunt lorem metus, ut congue ipsum congue at. " +
                "Nulla varius nec quam sit amet convallis. Nulla a posuere " +
                "ligula. Aenean sem odio, consectetur in molestie nec, " +
                "sodales vitae massa. Etiam in mollis tortor, gravida " +
                "aliquet nunc. Integer eu neque vitae diam laoreet commodo " +
                "semper sed felis.",
    price="0.69",
    sku="007",
    url="milk",
    cat_url="dairy-products"
    )

session.add(milk)
yogurt = CatItem(
    name="yogurt",
    description="Aliquam interdum nisi augue, ut malesuada nisi egestas non." +
                "Integer tincidunt lorem metus, ut congue ipsum congue at. " +
                "Nulla varius nec quam sit amet convallis. Nulla a posuere " +
                "ligula. Aenean sem odio, consectetur in molestie nec, " +
                "sodales vitae massa. Etiam in mollis tortor, gravida " +
                "aliquet nunc. Integer eu neque vitae diam laoreet commodo " +
                "semper sed felis.",
    price="1.50",
    sku="008",
    url="yogurt",
    cat_url="dairy-products"
    )

session.add(yogurt)
cheese = CatItem(
    name="cheese",
    description="Aliquam interdum nisi augue, ut malesuada nisi egestas non." +
                "Integer tincidunt lorem metus, ut congue ipsum congue at. " +
                "Nulla varius nec quam sit amet convallis. Nulla a posuere " +
                "ligula. Aenean sem odio, consectetur in molestie nec, " +
                "sodales vitae massa. Etiam in mollis tortor, gravida " +
                "aliquet nunc. Integer eu neque vitae diam laoreet commodo " +
                "semper sed felis.",
    price="1.80",
    sku="009",
    url="cheese",
    cat_url="dairy-products"
    )

session.add(cheese)
chicken = CatItem(
    name="chicken",
    description="Aliquam interdum nisi augue, ut malesuada nisi egestas non." +
                "Integer tincidunt lorem metus, ut congue ipsum congue at. " +
                "Nulla varius nec quam sit amet convallis. Nulla a posuere " +
                "ligula. Aenean sem odio, consectetur in molestie nec, " +
                "sodales vitae massa. Etiam in mollis tortor, gravida " +
                "aliquet nunc. Integer eu neque vitae diam laoreet commodo " +
                "semper sed felis.",
    price="1.50",
    sku="010",
    url="chicken",
    cat_url="meat"
    )

session.add(chicken)
beef = CatItem(
    name="beef",
    description="Aliquam interdum nisi augue, ut malesuada nisi egestas non." +
                "Integer tincidunt lorem metus, ut congue ipsum congue at. " +
                "Nulla varius nec quam sit amet convallis. Nulla a posuere " +
                "ligula. Aenean sem odio, consectetur in molestie nec, " +
                "sodales vitae massa. Etiam in mollis tortor, gravida " +
                "aliquet nunc. Integer eu neque vitae diam laoreet commodo " +
                "semper sed felis.",
    price="0.95",
    sku="011",
    url="beef",
    cat_url="meat"
    )

session.add(beef)
pork = CatItem(
    name="pork",
    description="Aliquam interdum nisi augue, ut malesuada nisi egestas non." +
                "Integer tincidunt lorem metus, ut congue ipsum congue at. " +
                "Nulla varius nec quam sit amet convallis. Nulla a posuere " +
                "ligula. Aenean sem odio, consectetur in molestie nec, " +
                "sodales vitae massa. Etiam in mollis tortor, gravida " +
                "aliquet nunc. Integer eu neque vitae diam laoreet commodo " +
                "semper sed felis.",
    price="3.50",
    sku="012",
    url="pork",
    cat_url="meat"
    )

session.add(pork)
session.commit()

print "added catalog app items!"
